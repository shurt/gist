# run linter

FROM node:8-alpine as lint

COPY . /var/app

WORKDIR /var/app

RUN npm i \
  && npm run lint

# transform with Babel

FROM node:8-alpine as babel

COPY --from=lint /var/app /var/app

WORKDIR /var/app

RUN npm run babel

# perform "production" build for optimizing your container

FROM node:8-alpine

ENV NODE_ENV="production" \
  BROWSER="chrome"

COPY --from=babel ["/var/app/dist", "/var/app/package.json", "/var/app/package-lock.json", "/var/app/"]

WORKDIR /var/app

RUN npm i

CMD ["node", "index.js"]
