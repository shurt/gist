var webdriverio = require('webdriverio');
var options = {
    desiredCapabilities: {
        browserName: 'chrome'
    },
    host: 'localhost',
    port: 4444,
    path: '/wd/hub'
};
webdriverio
 .remote(options)
 .init()
 .url('https://duckduckgo.com/')
 .setValue('#search_form_input_homepage', 'WebdriverIO')
 .click('#search_button_homepage')
 .saveScreenshot('screenshot.jpg')
 .getTitle()
 .then(function (title) {
   console.log('Title is: ' + title);
 })
 .end()
 .catch(function(err) {
     console.log(err);
 });